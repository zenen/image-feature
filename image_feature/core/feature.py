from dataclasses import dataclass
from typing import *

import numpy


class FeatureJson(TypedDict):
    dim: int
    feature: List[float]


@dataclass
class Feature:
    dim: int
    data: numpy.ndarray

    def to_json(self) -> FeatureJson:
        return {
            "dim": self.dim,
            "feature": self.data.tolist()
        }
