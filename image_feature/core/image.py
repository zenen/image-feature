from dataclasses import dataclass
from typing import *
import numpy

@dataclass
class Image:
    width: int
    height: int
    channel: int
    data: numpy.ndarray
