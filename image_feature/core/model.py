from typing import *
from image_feature.core.feature import Feature

from image_feature.core.image import Image


class Model(Protocol):
    def predict(self, image: Image) -> Feature:
        pass


ModelFactory = Callable[[], Model]


class ModelDict(Dict[str, ModelFactory]):
    
    def add(self, key: str, value: ModelFactory):
        self.update({ key: value })
