from fastapi.requests import Request

from image_feature.http.model_middleware import ModelMiddleware

def test_model_path_param_is_not_setted_respond_bad_request():
    middle = ModelMiddleware({ "model": lambda _: None })
    
    req = Request({ "type": "http", "path_params": { "model": None } })
    res = middle.__call__(req)

    assert res.status_code == 400

def test_model_dict_has_no_mached_key_respond_not_found():
    middle = ModelMiddleware({ "awesome_model": lambda _: None })
    
    req = Request({ "type": "http", "path_params": { "model": "ramdom_model" } })
    res = middle.__call__(req)

    assert res.status_code == 404
