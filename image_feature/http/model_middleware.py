from typing import Optional
from fastapi import Request, Response
from image_feature.core.model import ModelDict
from image_feature.http.response import bad_request, not_found


ModelNotSpecifiedError = "`model` param is required."


ModelNotFoundError = "specified model is not found."


class ModelMiddleware():
    
    def __init__(self, model_dict: ModelDict):
        self.models = model_dict
    
    def __call__(self, request: Request) -> Optional[Response]:
        model_key = request.path_params.get("model")
        if model_key is None:
            return bad_request(ModelNotSpecifiedError)

        model = self.models.get(model_key)
        if model is None:
            return not_found(ModelNotFoundError)

        request.state.model = model()
        return None
        