from typing import Optional
from fastapi import Request, Response
import numpy
from starlette.datastructures import UploadFile
import PIL.Image
from image_feature.core.image import Image

from image_feature.http.response import bad_request


ImageNotAttachedError = "image file is not attached."

InvalidImageError = "image file is invalid."


def from_pil_image(img: PIL.Image.Image) -> Image:
    return Image(img.width, img.height, len(img.getbands()), numpy.array(img))


class ImageMiddleware:

    async def __call__(self, request: Request) -> Optional[Response]:
        image_file = (await request.form()).get('file')
        if image_file is None or type(image_file) is not UploadFile:
            return bad_request(ImageNotAttachedError)
        try:    
            image = PIL.Image.open(image_file.file)
            image = image.convert("RGB")
            request.state.image = from_pil_image(image)
            return None
        except PIL.UnidentifiedImageError:
            return bad_request(InvalidImageError)
