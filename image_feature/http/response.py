from typing import Any
from fastapi.responses import JSONResponse


def bad_request(reason: str):
    return json_resp(error_msg_json(reason), 400)


def not_found(reason: str):
    return json_resp(error_msg_json(reason), 404)


def json_resp(msg: Any, status_code: int):
    return JSONResponse(msg, status_code)


def error_msg_json(reason: str):
    return {
        "ok": False,
        "reason": reason
    }
