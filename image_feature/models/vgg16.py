import torch
from torchvision.models import vgg16
from torchvision.models.vgg import VGG16_Weights
from image_feature.core.feature import Feature
from image_feature.core.image import Image

from image_feature.core.model import Model

WEIGHTS = VGG16_Weights.IMAGENET1K_FEATURES
vgg_model = vgg16(weights=WEIGHTS)

class Vgg16(Model):
    @classmethod
    def new(cls):
        return Vgg16()
    
    def predict(self, image: Image) -> Feature:
        tensor = torch.tensor(image.data.transpose(-1, 0, 1), requires_grad=False)
        tensor = WEIGHTS.transforms(antialias=True).forward(tensor)
        tensor = vgg_model.features(tensor.unsqueeze(0))
        tensor = torch.nn.AdaptiveMaxPool2d((1, 1))(tensor)
        tensor = torch.flatten(tensor)
        return Feature(tensor.size()[-1], tensor.detach().cpu().numpy()) 
