FROM python:3.10.11-slim-buster

WORKDIR /app
COPY . .

RUN python -m pip install -r requirements.txt

EXPOSE 80

CMD [ "uvicorn", "serve:app", "--host", "0.0.0.0", "--port", "80" ]
