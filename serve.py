from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse, Response
from image_feature.core.image import Image

from image_feature.core.model import Model, ModelDict
from image_feature.http.image_middleware import ImageMiddleware
from image_feature.http.model_middleware import ModelMiddleware
from image_feature.models.vgg16 import Vgg16

def get_feature(request: Request):
    assert request.state.model != None
    assert request.state.image != None
    model: Model = request.state.model
    image: Image = request.state.image
    feature = model.predict(image)
    return JSONResponse(feature.to_json())

models = ModelDict()
models.add("vgg", Vgg16.new)

app = FastAPI()
    
@app.route("/{model}/feature", ['POST'])
async def post(request: Request):
    res = ModelMiddleware(models)(request)
    if res is not None:
        return res
    res = await ImageMiddleware()(request)
    if res is not None:
        return res
    return get_feature(request)


@app.route("/health", ['GET'])
def health(_):
    return Response()
